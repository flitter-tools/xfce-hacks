# xfce-hacks

Couple of hacks for xfce, in search of ideal DE.

### Why?

I was suffered of some things in XFCE, but anyway it is best DE ever! The thing is you can't actually use it as standalone, without Thunar and friends. Also some things... Ah, just strange.

### Hacks

- ~~XFWM hacks/CompositorHack.diff -- make compositor "bypass" (actually not) root window, so you can install wallpaper with tool you like (hsetroot and such), without need of use Thunar and XFDesktop.~~ (depricated, xfce doing it by theyself)
- XFWM hacks/AltTabWinSize.diff -- make alt-tab icons (and previews) smaller.
- Panel hacks:
	~~- Sanespacing patch: actually not my work, I just found it in AUR, original author is Jordan Maris, but I change the value of spacing between icons, so just put it there.~~ (depricated, this was fixed in 4.14.1)
	- Swap arrows when tray is hidden
	- Show desktop window 7 style: make widget do not display any icon and make it half width minus 5px
- Whiskermenu hacks/RemoveProfilePicAndUsername.diff -- remove username and profile picture from Whiskermenu. To make resize widget right align I left empty label widget.
- ~~Clipman patch to not steal selections, based on work of [ConnorBehan](https://aur.archlinux.org/packages/xfce4-clipman-plugin-passive/) and PKGBUILD from [eworm](https://aur.archlinux.org/packages/xfce4-clipman-plugin-git/). Patch was fixed by myself.~~ (depricated, not needed)

![menu](screenshots/menu.png)

### Styles

gtk3.css remove ugly dash line, when scrolling in various scrolling area.

Still need to work on this, but it MUCH better then vanilla.

Put style to ~/.config/gtk-3.0

![Example of look](screenshots/panel.png)
